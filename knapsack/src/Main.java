import generator.ItemsSet;
import solutions.BruteForce;
import solutions.Dynamic;
import solutions.GreedyFractionnal;
import solutions.GreedyRatio;
import solutions.GreedyValue;
import solutions.GreedyWeight;

public class Main {
    public static void main(String[] args) throws Exception {
        final int MAX_WEIGHT = 100;

        ItemsSet items = new ItemsSet();

        System.out.println("ITEMS SET");
        System.out.println(items.toString());

        System.out.println("DYNAMIC SOLUTION");
        int res = Dynamic.knapsackDP(items, items.getItemArr().size(), MAX_WEIGHT);
        System.out.println("res = " + res);

        System.out.println("BRUTE FORCE SOLUTION");
        int bruteForce = BruteForce.knapsackBF(items, items.getItemArr().size(), MAX_WEIGHT);
        System.out.println("res = " + bruteForce);

        System.out.println("GREEDYWEIGHT SOLUTION");
        Double greedyweight = GreedyWeight.knapsackGF(items, items.getItemArr().size(), MAX_WEIGHT);
        System.out.println("res = " + greedyweight);

        System.out.println("GREEDYVALUE SOLUTION");
        Double greedyvalue = GreedyValue.knapsackGF(items, items.getItemArr().size(), MAX_WEIGHT);
        System.out.println("res = " + greedyvalue);

        System.out.println("GREEDYRATIO SOLUTION");
        Double greedyratio = GreedyRatio.knapsackGF(items, items.getItemArr().size(), MAX_WEIGHT);
        System.out.println("res = " + greedyratio);

        System.out.println("GREEDYFRACTIONAL SOLUTION");
        Double greedyfractional = GreedyFractionnal.knapsackGF(items, items.getItemArr().size(), MAX_WEIGHT);
        System.out.println("res = " + greedyfractional);

    }
}
