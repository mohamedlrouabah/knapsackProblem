package generator;

import java.util.Random;

public class ExponentialGenerator {
    public static Random rand = new Random();
    public static Double lampda = 1.0;

    public double getNext() {
        return Math.log(1 - rand.nextDouble()) / (-lampda);
    }

}
