package generator;

public class Item implements IGenerator {

    private int weight;
    private int value;

    public Item(int w, int v) {
        weight = w;
        value = v;
    }

    public Item() {
        // generate random values from 0-UPPERBOUND
        this(rand.nextInt(UPPERBOUND) + 1, rand.nextInt(UPPERBOUND) + 1);
    }

    public int getWeight() {
        return this.weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getValue() {
        return this.value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public Item weight(int weight) {
        setWeight(weight);
        return this;
    }

    public Item value(int value) {
        setValue(value);
        return this;
    }

    @Override
    public String toString() {
        return "{" +
                " weight='" + getWeight() + "'" +
                ", value='" + getValue() + "'" +
                "}";
    }

}
