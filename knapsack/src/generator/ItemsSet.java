package generator;

import java.util.ArrayList;

public class ItemsSet implements IGenerator {
    private ArrayList<Item> itemArr = new ArrayList<>();

    public ItemsSet() {
        for (int i = 0; i < MAX_ITEMS; i++)
            itemArr.add(new Item());
    }

    public ItemsSet(ArrayList<Item> itemArr) {
        this.itemArr = itemArr;
    }

    public ArrayList<Item> getItemArr() {
        return this.itemArr;
    }

    public void setItemArr(ArrayList<Item> itemArr) {
        this.itemArr = itemArr;
    }

    public ItemsSet itemArr(ArrayList<Item> itemArr) {
        setItemArr(itemArr);
        return this;
    }

    @Override
    public String toString() {
        String str = "\n";
        for (Item i : getItemArr())
            str += "\t" + i.toString() + "\n";

        return "{" + str + "}";
    }

}
