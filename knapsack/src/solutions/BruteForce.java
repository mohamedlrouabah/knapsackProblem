package solutions;

import generator.ItemsSet;

public class BruteForce {

    /**
     * @param items      : the arras of items
     * @param nbElem     : the number of items
     * @param PoidSacado : the maximal Weight
     * @return int : the result
     */
    public static int knapsackBF(ItemsSet items, int nbElem, int PoidSacado) {
        int resultat, a, b;
        /* if weigh is null and no items then return null */
        if (PoidSacado == 0 || nbElem == 0) {
            resultat = 0;
            return resultat;
        }
        /* check if weight PoidSacado is more than nth item */
        else if (items.getItemArr().get(nbElem - 1).getWeight() > PoidSacado) {
            /* if true return can't be included so pass the to next item */
            return knapsackBF(items, nbElem - 1, PoidSacado);
        }
        /* add into A the nth item included */
        a = items.getItemArr().get(nbElem - 1).getValue()
                + knapsackBF(items, nbElem - 1, PoidSacado - items.getItemArr().get(nbElem - 1).getWeight());
        /* add into B the nth item not included */
        b = knapsackBF(items, nbElem - 1, PoidSacado);
        /* maximum value that can be put in knapsack of Weight -> PoidSacado */
        resultat = Math.max(a, b);
        return resultat;
    }
}
