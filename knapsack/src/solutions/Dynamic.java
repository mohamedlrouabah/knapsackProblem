package solutions;

import generator.ItemsSet;

public class Dynamic {

    /**
     * @param items : the arras of items
     * @param n     : the number of items
     * @param W     : the maximal Weight
     * @return int : the result
     */
    public static int knapsackDP(ItemsSet items, int n, int W) {
        if (n <= 0 || W <= 0)
            return 0;

        int[][] m = new int[n + 1][W + 1];
        for (int j = 0; j <= W; j++) {
            m[0][j] = 0;
        }

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= W; j++) {
                if (items.getItemArr().get(i - 1).getWeight() > j) {
                    m[i][j] = m[i - 1][j];
                } else {
                    m[i][j] = Math.max(
                            m[i - 1][j],
                            m[i - 1][j - items.getItemArr().get(i - 1).getWeight()]
                                    + items.getItemArr().get(i - 1).getValue());
                }
            }
        }
        return m[n][W];
    }
}
