package solutions;

import generator.ItemsSet;

public class GreedyRatio {

    public static Double knapsackGF(ItemsSet items, int object, int m) {

        double p_w[] = new double[object];
        for (int i = 0; i < object; i++) {
            p_w[i] = (double) items.getItemArr().get(i).getValue() / (double) items.getItemArr().get(i).getWeight();
        }
        System.out.println("");
        System.out.println("-------------------");
        System.out.println("-----Data-Set------");
        System.out.print("-------------------");
        System.out.println("");
        System.out.print("Objects");
        for (int i = 1; i <= object; i++) {
            System.out.print(i + "    ");
        }
        System.out.println();
        System.out.print("Profit ");
        for (int i = 0; i < object; i++) {
            System.out.print(items.getItemArr().get(i).getValue() + "    ");
        }
        System.out.println();
        System.out.print("Weight ");
        for (int i = 0; i < object; i++) {
            System.out.print(items.getItemArr().get(i).getWeight() + "    ");
        }
        System.out.println();
        System.out.print("P/W    ");
        for (int i = 0; i < object; i++) {
            System.out.print(p_w[i] + "  ");
        }
        for (int i = 0; i < object - 1; i++) {
            for (int j = i + 1; j < object; j++) {
                if (p_w[i] < p_w[j]) {
                    double temp = p_w[j];
                    p_w[j] = p_w[i];
                    p_w[i] = temp;

                    int temp1 = items.getItemArr().get(j).getValue();
                    items.getItemArr().get(j).setValue(items.getItemArr().get(i).getValue());
                    items.getItemArr().get(i).setValue(temp1);

                    int temp2 = items.getItemArr().get(j).getWeight();
                    items.getItemArr().get(j).setWeight(items.getItemArr().get(i).getWeight());
                    items.getItemArr().get(i).setWeight(temp2);
                }
            }
        }
        System.out.println("");
        System.out.println("-------------------");
        System.out.println("--After Arranging--");
        System.out.print("-------------------");
        System.out.println("");
        System.out.print("Objects");
        for (int i = 1; i <= object; i++) {
            System.out.print(i + "    ");
        }
        System.out.println();
        System.out.print("Profit ");
        for (int i = 0; i < object; i++) {
            System.out.print(items.getItemArr().get(i).getValue());
        }
        System.out.println();
        System.out.print("Weight ");
        for (int i = 0; i < object; i++) {
            System.out.print(items.getItemArr().get(i).getWeight() + "    ");
        }
        System.out.println();
        System.out.print("P/W    ");
        for (int i = 0; i < object; i++) {
            System.out.print(p_w[i] + "  ");
        }
        int k = 0;
        double sum = 0;
        while (m > 0 && k < object) {
            if ((items.getItemArr().get(k).getWeight()) <= m) {

                sum += 1 * items.getItemArr().get(k).getValue();
                m = m - (items.getItemArr().get(k).getWeight());

            }
            k++;
        }
        return sum;

    }

}
