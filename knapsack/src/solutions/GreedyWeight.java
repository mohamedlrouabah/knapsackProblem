package solutions;

import generator.ItemsSet;

public class GreedyWeight {

    public static Double knapsackGF(ItemsSet items, int object, int m) {

        System.out.println("");
        System.out.println("-------------------");
        System.out.println("-----Data-Set------");
        System.out.print("-------------------");
        System.out.println("");
        System.out.print("Objects");
        for (int i = 1; i <= object; i++) {
            System.out.print(i + "    ");
        }
        System.out.println();
        System.out.print("Profit ");
        for (int i = 0; i < object; i++) {
            System.out.print(items.getItemArr().get(i).getValue() + "    ");
        }
        System.out.println();
        System.out.print("Weight ");
        for (int i = 0; i < object; i++) {
            System.out.print(items.getItemArr().get(i).getWeight() + "    ");
        }

        for (int i = 0; i < object - 1; i++) {
            for (int j = i + 1; j < object; j++) {
                if (items.getItemArr().get(i).getWeight() < items.getItemArr().get(j).getWeight()) {

                    int temp1 = items.getItemArr().get(j).getValue();
                    items.getItemArr().get(j).setValue(items.getItemArr().get(i).getValue());
                    items.getItemArr().get(i).setValue(temp1);

                    int temp2 = items.getItemArr().get(j).getWeight();
                    items.getItemArr().get(j).setWeight(items.getItemArr().get(i).getWeight());
                    items.getItemArr().get(i).setWeight(temp2);
                }
            }
        }
        System.out.println("");
        System.out.println("-------------------");
        System.out.println("--After Arranging--");
        System.out.print("-------------------");
        System.out.println("");
        System.out.print("Objects");
        for (int i = 1; i <= object; i++) {
            System.out.print(i + "    ");
        }
        System.out.println();
        System.out.print("Profit ");
        for (int i = 0; i < object; i++) {
            System.out.print(items.getItemArr().get(i).getValue() + "    ");
        }
        System.out.println();
        System.out.print("Weight ");
        for (int i = 0; i < object; i++) {
            System.out.print(items.getItemArr().get(i).getWeight() + "    ");
        }
        int k = 0;
        double sum = 0;
        while (m > 0 && k < object) {
            if ((items.getItemArr().get(k).getWeight()) <= m) {

                sum += 1 * items.getItemArr().get(k).getValue();
                m = m - (items.getItemArr().get(k).getWeight());

            }

            k++;
        }
        return sum;

    }

}
